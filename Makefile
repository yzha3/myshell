.DELETE_ON_ERROR:

MYSH_CMD := ls\
			echo\
			mkdir\
			cat\
			rm\
			wc

MYSH_OBJ := $(addprefix bin/,$(MYSH_CMD))

#all: mysh bin $(MYSH_OBJ)

all: mysh

bin:
	mkdir -p bin

mysh: ./source/mysh.c
	gcc  -o mysh ./source/mysh.c

bin/%: source/%.c 
	gcc -o $@ $<

clean:
	rm -f mysh

#rm -fr ./bin

gdb:
	gcc -g -o mysh ./source/mysh.c
