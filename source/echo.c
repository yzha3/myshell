/*************************************************************
	>File Name: source/echo.c
	>Author: Yue
	>Created Time: Wed 17 Sep 2014 06:34:35 PM CDT
	>Description:
*************************************************************/

#include<stdio.h>

int main(int argc,char *argv[])
{
	int i=0;
	while (argv[++i]!=NULL) fprintf (stdout,"%s%s",argv[i],(i==argc-1)?"\n":" ");
	return 0;
}
