/*************************************************************
	>File Name: source/cat.c
	>Author: Yue
	>Created Time: 2014年09月19日 星期五 09时48分43秒
	>Description: cat command, support no option
*************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>

int main(int argc,char *argv[])
{
	char buf[1024];
	int n,i,f_d;

	if (argc==1)
	{
		while ((n=read(0,buf,sizeof(buf)))>0)
			write(1,buf,n);
		if (n<0)
		{
			fprintf(stderr,"Error!\n");
			exit(-1);
		}
	}
	else
		for (i=1;i<argc;i++)
		{
			f_d=open(argv[i],0);
			if (f_d<0) 
			{
				fprintf(stderr,"Error!\n");
				exit (-1);
			}
			while((n=read(f_d,buf,sizeof(buf)))>0)
				write(1,buf,n);
			if (n<0)
			{
				fprintf(stderr,"Error!\n");
				exit (-1);
			}
			close(f_d);
		}
	return 0;
}
