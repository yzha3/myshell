/*************************************************************
	>File Name: source/mkdir.c
	>Author: Yue
	>Created Time: Wed 17 Sep 2014 08:31:21 PM CDT
	>Description:
*************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<getopt.h>
int main(int argc,char *argv[])
{ 
	int mode=0775,i,opt,dir=0;
	opterr=0;
	while ((opt=getopt(argc,argv,"m:"))!=-1)
		if (opt=='m') 
			if (sscanf(optarg,"%o",&mode)!=1)
			{
				fprintf(stderr,"Error!\n");
				exit(1);
			}
	i=1;
	while (i<argc)
		if (argv[i][0]=='-') i=i+2;
		else
			if(mkdir(argv[i++],mode)<0)
				fprintf(stderr,"Error!\n");
			else dir=1;
	if (dir==0) fprintf(stderr,"Error!\n");
	return 0;
}
