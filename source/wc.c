/*************************************************************
	>File Name: wc.c
	>Author: Yue
	>Created Time: 2014年09月19日 星期五 15时43分04秒
	>Description:
*************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>

static int total_l,total_w,total_c;
void 
wc(char *name)
{
	int fd,n,l_c=0,w_c=0,c_c=0,i;
	char store[1024];

	if (name==NULL) fd=0;
	else fd=open(name,0);

	if (fd<0) 
	{
		fprintf(stderr,"Error!\n");
		return;
	}
	
	while((n=read(fd,store,sizeof(store)))>0)
	{
		for (i=0;i<n;i++)
		{
			c_c++;
			if (store[i]=='\n') l_c++;		
		}
		if (strtok(store," \n\t\r")!=NULL) 
		{
			w_c++;
			while (strtok(NULL," \n\t\r")!=NULL) w_c++;
		}
		if (l_c==1) w_c--;
	}
	if (n<0) 
	{
		fprintf(stderr,"Error!\n");
		return;
 	}
	total_l+=l_c;
	total_w+=w_c;
	total_c+=c_c;
	if (name!=NULL)close(fd);
	fprintf(stdout,"\t%d\t%d\t%d\t%s\n",l_c,w_c,c_c,(name==NULL)?"":name);
} 
int main(int argc,char *argv[])
{
	total_l=0;total_w=0;total_c=0;
	int i;
	wc(argv[1]);
	for (i=2;i<argc;i++)
		wc(argv[i]);
	if (argv[1]!=NULL) fprintf(stdout,"\t%d\t%d\t%d\ttotal\n",total_l,total_w,total_c);
	return 0;
}
