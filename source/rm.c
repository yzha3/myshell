/*************************************************************
	>File Name: source/rm.c
	>Author: Yue
	>Created Time: 2014年09月18日 星期四 17时59分37秒
	>Description:
*************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#include<dirent.h>
#include<sys/types.h>

int delete(char *name)
{
	DIR *dp=opendir(name);
	struct dirent *dirp;
	char path[100];
	strcpy(path,name);

	if (dp==NULL) return 1;
	while ((dirp=readdir(dp))!=NULL)
	{
		if(strcmp(dirp->d_name,".")!=0&&strcmp(dirp->d_name,"..")!=0)
		{
			if(dirp->d_type==4)
			{
				strcat(path,dirp->d_name);
				strcat(path,"/");
				delete(path);
				if (rmdir(path)<0) return -1;
				strcpy(path,name);
 			}
			else
			{ 
				strcat(path,dirp->d_name);
				if (unlink(path)<0) return -1;
				
				strcpy(path,name);
 			}
		}
 	}
	return 0;
}
int main(int argc,char *argv[])
{
	int i=0;
	if (strcmp(argv[1],"-r")==0)
	{
		i=1;
		while (argv[++i]!=NULL)
		{
			if (argv[i][strlen(argv[i])-1]!='/') strcat(argv[i],"/");
			switch (delete(argv[i]))
			{
				case 0:
					if (rmdir(argv[i])<0) fprintf(stderr,"Error!\n");
					break;
				case 1:
					argv[i][strlen(argv[i])-1]='\0';
					if (unlink(argv[i])<0) fprintf(stderr,"Error!\n");
					break;
				default:
					fprintf(stderr,"Error!\n");
					break;
			}
		}
	}
	else 
		while (argv[++i]!=NULL)
			if(unlink(argv[i])<0) fprintf(stderr,"Error!\n");
	return 0;
}
