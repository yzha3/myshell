/*************************************************************
	>File Name: ls.c
	>Author: Yue
	>Created Time: Wed 17 Sep 2014 02:39:21 PM CDT
	>Description:
*************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<dirent.h>
#include<sys/types.h>
#include<string.h>
#include<stdbool.h>
int main(int argc,char *argv[])
{
	DIR	*dp;
	struct dirent *dirp;
	char dirname[30]="./"; 
	int i,j=0;
	bool option[2]={false,false};
	while (++j<argc)
	{
		if (argv[j][0]=='-') {
			i=1;
			while (argv[j][i]!='\0')
				switch( argv[j][i++])
				{
					case 'l':
						option[0]=true;
						break;
					case 'a':
						option[1]=true;
						break;
					default:
						break;
				}
		}
		else strcpy(dirname,argv[j]);
	}
	if ((dp=opendir(dirname))==NULL)
	{
		fprintf(stderr,"ls: cannot access ");
		perror(argv[1]);
	}
	while ((dirp=readdir(dp))!=NULL)
	{
		if (dirp->d_name[0]!='.'||option[1]) 
		{
			printf ("%s",dirp->d_name);
			if (dirp->d_type==4) printf ("%c",'/');
			if (option[0]) printf("\n");
			else printf ("  ");
		}
	}
	if (!option[0]) printf("\n");
	closedir(dp);
	return 0;
}
