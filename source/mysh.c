/*************************************************************
	>File Name: mysh.c
	>Author: Yue
	>Created Time: Wed 17 Sep 2014 03:32:36 PM CDT
	>Description: My shell
*************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<stddef.h>
#include<string.h>
#include<fcntl.h>

#define MAX_ARGS 20 //Max number for ARGV per command

struct CMD
{   
	char *argv[MAX_ARGS];
	int TYPE;
	char input[1024];
	char output[1024];
	int MODE;
	struct CMD *pipecmd;
};    
char *read_cmd(void)	//read command from user input, this will repeat when get a not NULL cmd, and add it to history
{   
	char cmd[1024];
	int i=0;
	fprintf(stdout,"mysh> ");
	while (fgets(cmd,1024,stdin)==NULL) fprintf(stdout,"mysh> ");
	cmd[strlen(cmd)-1]='\0';
	while(cmd[i]!='\0'&&(cmd[i]==' '||cmd[i]=='\t')) i++;
	while (cmd[i]=='\0')
	{   
		fprintf(stdout,"mysh> ");
		while (fgets(cmd,1024,stdin)==NULL) fprintf(stdout,"mysh> ");
		i=0;
		cmd[strlen(cmd)-1]='\0';
		while(cmd[i]!='\0'&&(cmd[i]==' '||cmd[i]=='\t')) i++;
	}
	char * returncmd=malloc(sizeof(*returncmd));
	memset(returncmd,0,sizeof(*returncmd));
	strcpy(returncmd,cmd);
	return returncmd;
}
struct CMD * parse_cmd(char * cmd_copy)//split the command
 { 
	char cmd[1024];
	strcpy(cmd,cmd_copy);
	struct CMD *return_value=malloc(sizeof(*return_value));
	memset(return_value,0,sizeof(*return_value));
	char cmd_part[1024],*argv_temp,file_temp[1024];
	int i,args,j,k;
	return_value->TYPE=0;
	return_value->input[0]=0;
	return_value->output[0]=0;
	return_value->pipecmd=NULL;
	return_value->MODE=-1;
	for (i=0;i<MAX_ARGS;i++) return_value->argv[i]=NULL;
	//set initial values

	i=0;
	j=0;
	while( i<strlen(cmd))
  	{   
		//Check pipe and then break
		if (cmd[i]=='|') 
 	  	 {
			return_value->TYPE=1;
			i++;
			break;
	  	} 
		//check file redirection
		if (cmd[i]=='<') 
		{ 
			k=0;
			while(cmd[++i]==' ');
			while(cmd[i]!=' '&&cmd[i]!='\0'&&cmd[i]!='<'&&cmd[i]!='|'&&cmd[i]!='>'&& k<1024)
				file_temp[k++]=cmd[i++];
			file_temp[k]='\0';
			strcpy(return_value->input,file_temp);
			continue;
 	 	} 
	 	if (cmd[i]=='>')
	  	{   
		 	if (cmd[i+1]=='>')
		   	{
				i++;
				return_value->MODE=1; //append
 	  		}
	 		else return_value->MODE=0;//overwrite
			k=0;
			while(cmd[++i]==' ');
			while(cmd[i]!=' '&&cmd[i]!='\0'&&cmd[i]!='<'&&cmd[i]!='|'&&cmd[i]!='>'&&k<1024)
				file_temp[k++]=cmd[i++];
			file_temp[k]='\0';
			strcpy(return_value->output,file_temp);
			continue;
 		}
		cmd_part[j++]=cmd[i++];
 	 }   
	cmd_part[j]='\0';
	//parse the cmd and set argv
	args=0;
	argv_temp=malloc(sizeof(file_temp));
	if ((argv_temp=strtok(cmd_part," \t"))!=NULL)
  	{
		return_value->argv[0]=malloc(sizeof(file_temp));
		memset(return_value->argv[0],0,sizeof(file_temp));
		strcpy(return_value->argv[args++],argv_temp);
	
		while((argv_temp=strtok(NULL," \t"))!=NULL)
  		{ 
			return_value->argv[args]=malloc(sizeof(file_temp));
			memset(return_value->argv[args],0,sizeof(file_temp));
			strcpy(return_value->argv[args++],argv_temp);
		}
	} 
	
	//if it is pipe cmd, repeat and check the next part of the command
	if (return_value->TYPE==1) 
 	{  
		j=0;
		do
			cmd_part[j++]=cmd[i];
		while (cmd[i++]!='\0');
		return_value->pipecmd=parse_cmd(cmd_part);
 	 }  
	return (struct CMD*) return_value;
}    
int built_in_command(struct CMD *cmd) //check exit command
{
	if (cmd->argv[0]==NULL)
	{
		fprintf(stderr,"Error!\n");
		return -1;
	}
	if (strcmp(cmd->argv[0],"exit")==0)
	{ 
		if (cmd->argv[1]!=NULL||cmd->input[0]!=0||cmd->output[0]!=0||cmd->pipecmd!=NULL)
	 	{
			fprintf(stderr,"Error!\n");
			return -1;
		}
		exit (0);
	}
	else if(strcmp(cmd->argv[0],"cd")==0)
	{
		if (cmd->argv[1]==NULL) 
		{
			chdir("./");
			return 1;
		}
		else
			if (chdir(cmd->argv[1])!=0)
	 		{
				fprintf(stderr,"Error!\n");
				return -1;
			}
		return 1;
	}
	else return 0;
}
void run_cmd(struct CMD *cmd,int PIPE0,int PIPE1) //run command
{ 
	
	int child_pid,p[2],fdi=-1,fdo=-1;
	char cwd[1024];
	if (cmd->argv[0]==NULL) 
 	{
		fprintf(stderr,"Error!\n");
		return;
	}
	if (cmd->TYPE==1) pipe(p); //Start of a pipe

	child_pid=fork();
	if (child_pid==0)
	{
		if (cmd->TYPE==1) //the cmd is a left end of a pipe
		{ 
			dup2(p[1],1);
			close(p[0]);
		}
		if (PIPE0!=-1)	//the cmd is a right end of another pipe
	  	{
			dup2(PIPE0,0);
			close(PIPE1);
		}
		if (cmd->input[0]!=0) //input file rediretion
		{ 
			//close(0);
			if ((freopen(cmd->input,"r",stdin))==NULL)
			{
				fprintf(stderr,"Error!\n");
				exit (-1);
 			}
 		}   
		if (cmd->output[0]!=0)  //output file rediretion
 	 	{   
			//close(1);
		 	if (cmd->MODE==0) 
			 { 
				if ((freopen(cmd->output,"w",stdout))==NULL)
					{
						fprintf(stderr,"Error!\n");
						exit (-1);
 			  		}	 //overwrite
 			}   
			else if (cmd->MODE==1)
			{
				if ((freopen(cmd->output,"a",stdout))==NULL)
					{ 
					fprintf(stderr,"Error!\n");
					exit(-1);
			 		}//append
			}  
			else
			{ 
				fprintf(stderr,"Error!\n");
				exit(-1);
		 	} 
		} 
		if (strcmp(cmd->argv[0],"pwd")==0)
		{
			getcwd(cwd,1024);
			fprintf(stdout,"%s\n",cwd);
			exit(0);
		}
		execvp(cmd->argv[0],cmd->argv);
		fprintf(stderr,"Error!\n");
		exit (-1);
 	 } 
	else
	{
		if (PIPE0!=-1) 
		{ 
			close(PIPE0);
			close(PIPE1);
 	  	}
		if (cmd->TYPE==1) run_cmd(cmd->pipecmd,p[0],p[1]); //if the cmd is a start of a pipe, then run the function again to execute the command on the other end of the pipe

		wait();
	} 
}   
int main(int argc,char *argv[])
{       
	struct CMD *cmd;
 	while(1)
	{   
		cmd=parse_cmd(read_cmd());
		if (built_in_command(cmd)==0)
		{
			run_cmd(cmd,-1,-1);
		}
 	 } 
	return 0;
}
